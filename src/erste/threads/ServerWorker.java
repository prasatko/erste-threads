package erste.threads;

import java.util.UUID;

import static erste.threads.Logger.log;
import static erste.threads.Sleep.sleepRandomSecs;

/**
 * Serverove vlakno.
 * <ul>
 * <li>vygeneruje sve jmeno</li>
 * <li>zaregistruje se do {@link ServerRegistry}</li>
 * <li>pocka 1-3s</li>
 * <li>publikuje sve jmeno do {@link ServerRegistry}</li>
 * <li>pocka 5-10s</li>
 * <li>odstrani sve jmeno z {@link ServerRegistry}</li>
 * <li>odregistruje se z {@link ServerRegistry}</li>
 * </ul>
 */
abstract class ServerWorker implements Runnable {
    private String privateName = UUID.randomUUID().toString();
    private ServerRegistry serverRegistry;

    /**
     *
     * @param serverRegistry reference na registr serveru, aby se server mohl zaregistrovat a odregistrovat.
     */
    ServerWorker(ServerRegistry serverRegistry) {
        this.serverRegistry = serverRegistry;
    }

    @Override
    public void run() {
        try {
            log("started");
            serverRegistry.registerServer(this);
            sleepRandomSecs(1, 3);
            serverRegistry.publishServerName(privateName);
            sleepRandomSecs(5, 10);
            serverRegistry.unpublishServerName(privateName);
            log("finished");
        } catch (InterruptedException e) {
            log("interrupted");
            Thread.currentThread().interrupt();
        } finally {
            serverRegistry.unregisterServer(this);
        }
    }

    /**
     * Vrati jmeno ve specifickem formatu (napr JSON nebo XML).
     * Pokud jmeno tohoto serveroveho vlakna neni publikovano v {@link ServerRegistry}, vrati null.
     * @return jmeno serveroveho vlakna nebo null.
     */
    String getName() {
        if (!serverRegistry.isServerNamePublished(privateName)) {
            return null;
        }
        return privateName;
    }

    /**
     * Vrati ciste jmeno serveroveho vlakna.
     * @return jmeno serveroveho vlakna
     */
    String getPrivateName() {
        return privateName;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "-" + getPrivateName();
    }
}

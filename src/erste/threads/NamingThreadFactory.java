package erste.threads;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Thread factory, ktera pojmenovava vlakna pomoci daneho prefixu a stale inktementujiciho se counteru.
 * Pro prehlednost.
 */
public class NamingThreadFactory implements ThreadFactory {
    private AtomicInteger count = new AtomicInteger(1);
    private String prefix;

    NamingThreadFactory(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public Thread newThread(Runnable r) {
        return new Thread(r, prefix + "-" + count.getAndIncrement());
    }
}

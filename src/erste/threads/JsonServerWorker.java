package erste.threads;

import java.util.Optional;

/**
 * Serverove vlakno, jehoz {@link #getName()} vraci jmeno v JSON strukture.
 */
class JsonServerWorker extends ServerWorker {
    JsonServerWorker(ServerRegistry serverRegistry) {
        super(serverRegistry);
    }

    @Override
    String getName() {
        return Optional.ofNullable(super.getName())
                .map(name -> "{\"name\": \"" + name + "\"}")
                .orElse(null);
    }
}

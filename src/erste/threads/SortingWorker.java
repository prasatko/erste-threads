package erste.threads;

import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Radici vlakno. Konzumuje frontu s prikazy pro vypsani serazeneho seznamu jmen serveru.
 *
 */
public class SortingWorker implements Runnable {
    private ServerRegistry serverRegistry;
    private BlockingQueue<SortingRequest> sortingRequestQueue = new LinkedBlockingQueue<>();


    /**
     *
     * @param serverRegistry registr serveru
     * @param sortingRequestQueue fronta na ktere bude radici vlakno poslouchat prikazy pro vypsani serazeneho seznamu jmen serveru.
     */
    SortingWorker(ServerRegistry serverRegistry, BlockingQueue<SortingRequest> sortingRequestQueue) {
        this.serverRegistry = serverRegistry;
        this.sortingRequestQueue = sortingRequestQueue;
    }

    @Override
    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                sortingRequestQueue.take();
                sort();
            }
        } catch (InterruptedException e) {
            Logger.log("interrupted");
            Thread.currentThread().interrupt();
        }
    }

    private void sort() {
        Logger.log("sorting requested");
        // sorting using a tree set:
        Set<String> sorted = new TreeSet<>(serverRegistry.getPublishedServerNames());
        Logger.log("sorted server names: " + sorted);
    }

    static class SortingRequest {
    }
}

package erste.threads;

import static java.util.Optional.ofNullable;

/**
 * Klientske vlakno. Zobrazi nahodny server z {@link ServerRegistry}.
 * Predpoklada se pravidelne spousteni pomoci {@link java.util.concurrent.ScheduledExecutorService}.
 *
 */
public class ClientWorker implements Runnable {
    private ServerRegistry serverRegistry;

    /**
     *
     * @param serverRegistry registr serveru
     */
    ClientWorker(ServerRegistry serverRegistry) {
        this.serverRegistry = serverRegistry;
    }

    @Override
    public void run() {
        ServerWorker serverWorker = serverRegistry.getRandomRunningServer();

        if (serverWorker != null) {
            // tady si nejsem jisty, zda jsme se pochopili - tim ze mam v ruce referenci na server nemusim prece vystup z getName() konsolidovat,
            // pouze se ujistim, jestli getName() nahodou nevratila null, pokud ano, vypisu chybu.
            Logger.log("random server name: " + ofNullable(serverWorker.getName())
                    .map(name -> name + " (private name: " + serverWorker.getPrivateName() + ")")
                    .orElse("error: server name not published!"));
        } else {
            Logger.log("random server name: no server");
        }
    }

}

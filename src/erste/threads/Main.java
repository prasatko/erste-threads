package erste.threads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static erste.threads.Logger.log;
import static erste.threads.Sleep.sleepRandomSecs;
import static java.lang.System.currentTimeMillis;

/**
 * Hlavni vlakno a vstup do aplikace.
 * <ul>
 * <li>Vyrobi frontu pro komunikaci mezi {@link ServerRegistry} a {@link SortingWorker}.
 *     Frontu jsem pouzil proto, aby jsem nemel mezi komponentama cyklickou zavislost a aby bylo mozne ridit prutok pozadavku na razeni.
 * </li>
 * <li>Vyrobi {@link ServerRegistry} - registr jmen serveru a bezicich serveru - sdilena struktura ze zadani.</li>
 * <li>Vyrobi {@link SortingWorker} - razeni seznamu jmen serveru.</li>
 * <li>Vyrobi {@link ClientWorker} - klientske vlakno.</li>
 * <li>Vyrobi nekolik instanci {@link ExecutorService} pro beh vlaken. Pro serverova vlakna jsem zvolil {@link ThreadPoolExecutor} s {@link SynchronousQueue},
 * coz zajisti omezeny pocet server vlaken bez frontovani - pokud neni volne vlakno, zaloguje se chyba, pozadavek na vytvoreni noveho server vlakna je ignorovan.
 * Pro klientske vlakno jsem zvolil {@link java.util.concurrent.ScheduledThreadPoolExecutor} aby bylo mozne jednoduse nacasovat jeho spousteni.
 * </li>
 * <li>Spusti {@link SortingWorker} a hlavni zpracovani - kazdych 1-5s spusti novy {@link ServerWorker}.</li>
 * </ul>
 *
 */
public class Main {

    private static final int POOLSIZE_SERVER = 100;
    private static final int SORTING_QUEUE_SIZE = 5;
    private final ExecutorService serverExecutor;
    private final ExecutorService sortingExecutor;
    private final SortingWorker sortingWorker;
    private final ServerRegistry serverRegistry;
    private final ScheduledExecutorService clientExecutor;
    private final ClientWorker clientWorker;


    private Main() {
        // fronta pro komunikaci mezi ServerRegistry a SortingWorker
        LinkedBlockingQueue<SortingWorker.SortingRequest> sortRequestQueue = new LinkedBlockingQueue<>(SORTING_QUEUE_SIZE);
        serverRegistry = new ServerRegistry(sortRequestQueue);
        sortingWorker = new SortingWorker(serverRegistry, sortRequestQueue);
        clientWorker = new ClientWorker(serverRegistry);
        // executors
        serverExecutor = new ThreadPoolExecutor(POOLSIZE_SERVER, POOLSIZE_SERVER, 0, TimeUnit.MILLISECONDS, new SynchronousQueue<>(), new NamingThreadFactory("server"));
        sortingExecutor = Executors.newSingleThreadExecutor(new NamingThreadFactory("sorting"));
        clientExecutor = Executors.newSingleThreadScheduledExecutor(new NamingThreadFactory("client"));
    }

    private void start() throws InterruptedException {
        sortingExecutor.submit(sortingWorker);
        clientExecutor.scheduleAtFixedRate(clientWorker,1, 1, TimeUnit.SECONDS);

        while(!Thread.currentThread().isInterrupted()) {
            sleepRandomSecs(1, 5);
            dispatchNewServerWorker();
        }

    }

    private void dispatchNewServerWorker() {
        // Vytvorim jeden z typu server workeru:
        ServerWorker serverWorker = currentTimeMillis() % 2 == 0 ? new JsonServerWorker(serverRegistry) : new XmlServerWorker(serverRegistry);
        try {
            serverExecutor.submit(serverWorker);
        } catch (RejectedExecutionException e) {
            log("Unable to dispatch new server worker - pool exhausted - ignoring");
        }

    }

    public static void main(String[] args) throws InterruptedException {
        new Main().start();
    }

}

package erste.threads;

final class Sleep {
    /**
     * Uspi aktualni vlakno na nahodny interval.
     * @param minSecsToSleep minimalni pocet sekund
     * @param maxSecsToSleep maximalni pocet sekund
     * @throws InterruptedException v pripade ze bylo behem spani vlakno preruseno (nemeni interrupted flag)
     */
    static void sleepRandomSecs(int minSecsToSleep, int maxSecsToSleep) throws InterruptedException {
        Thread.sleep((long) (minSecsToSleep*1000 + Math.random()*(maxSecsToSleep-minSecsToSleep)*1000));
    }
}

package erste.threads;

import java.util.Optional;

/**
 * Serverove vlakno, jehoz {@link #getName()} vraci jmeno v XML strukture.
 */
class XmlServerWorker extends ServerWorker {
    XmlServerWorker(ServerRegistry serverRegistry) {
        super(serverRegistry);
    }

    @Override
    String getName() {
        return Optional.ofNullable(super.getName())
                .map(name -> "<name>" + name + "</name>")
                .orElse(null);
    }
}

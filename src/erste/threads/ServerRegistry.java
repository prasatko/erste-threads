package erste.threads;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Registr serveru. Drzi si mnozinu zaregistrovanych serveru (pro ucely klientskeho vlakna) a mnozinu publikovanych jmen pro ucely radiciho vlakna.
 * Obe mnoziny jsou zalozeny na {@link ConcurrentHashMap}, takze cteni a zapis nejsou blokovany, je to thread-safe a obsah se muze behem cteni
 * zmenit, coz resime pouzitim snapshot kopie viz napr {@link #getPublishedServerNames()}.
 * <br>
 * Registr na nektere udalosti posila zpravu radicimu vlaknu pro vypsani serazeneho seznamu publikovanych jmen.
 */
class ServerRegistry {

    private final BlockingQueue<SortingWorker.SortingRequest> sortRequestQueue;
    private final Set<String> publishedServerNames = ConcurrentHashMap.newKeySet();
    private final Set<ServerWorker> runningServers = ConcurrentHashMap.newKeySet();
    private final Random random = new Random();

    /**
     * @param sortRequestQueue fronta pro komunikaci s {@link SortingWorker} - poslani prikazu k vypsani serazeneho seznamu jmen.
     */
    ServerRegistry(BlockingQueue<SortingWorker.SortingRequest> sortRequestQueue) {
        this.sortRequestQueue = sortRequestQueue;
    }


    /**
     * Zaregistruje server.
     * @param serverWorker server k zaregistrovani
     */
    void registerServer(ServerWorker serverWorker) {
        runningServers.add(serverWorker);
        Logger.log("registered server: " + serverWorker);
    }

    /**
     * Odregistruje server.
     * @param serverWorker server k odregistraci
     */
    void unregisterServer(ServerWorker serverWorker) {
        runningServers.remove(serverWorker);
        Logger.log("unregistered server: " + serverWorker);
    }

    /**
     * Prida jmeno serveru do mnoziny publikovanych jmen.
     * @param serverName jmeno serveru
     */
    void publishServerName(String serverName) {
        publishedServerNames.add(serverName);
        Logger.log("published server name: " + serverName);
        requestSorting();
    }

    /**
     * Odebere jmeno serveru z mnoziny publikovanych jmen.
     * @param serverName jmeno serveru
     */
    void unpublishServerName(String serverName) {
        publishedServerNames.remove(serverName);
        Logger.log("unpublished server name: " + serverName);
        requestSorting();
    }

    /**
     * Vrati kopii mnoziny publikovanych jmen.
     * @return mnozina publikovanych jmen
     */
    Set<String> getPublishedServerNames() {
        return new HashSet<>(publishedServerNames);
    }

    /**
     * Vrati nahodny bezici server.
     * @return nahodny server nebo null, pokud zadny neni zaregistrovany
     */
    ServerWorker getRandomRunningServer() {
        ArrayList<ServerWorker> list = new ArrayList<>(runningServers);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(random.nextInt(list.size()));
    }

    /**
     * Zkontroluje, zda je dane jmeno v mnozine publikovanych jmen.
     * @param serverName jmeno serveru
     * @return true pokud jmeno je v mnozine publikovanych jmen
     */
    boolean isServerNamePublished(String serverName) {
        return publishedServerNames.contains(serverName);
    }

    /**
       Posle radicimu vlaknu prikaz pro vypsani serazeneho seznamu jmen.
     */
    private void requestSorting() {
        if (!sortRequestQueue.offer(new SortingWorker.SortingRequest())) {
            Logger.log("unable to request logging, queue is full - ignoring");
        }
    }

}

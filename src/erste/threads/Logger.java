package erste.threads;

/**
 * Jednoduchy logger zapisujici vse na stdout. Bezne bych pouzil SLF4J, nechtel jsem do tohoto prikladu davat zavislosti.
 */
final class Logger {
    static void log(String msg, String... args) {
        System.out.println("[" + Thread.currentThread().getName() + "] " + msg);
    }
}
